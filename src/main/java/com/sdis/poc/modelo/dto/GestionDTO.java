package com.sdis.poc.modelo.dto;

/**
 * This class was automatically generated by the data modeler tool.
 */

@org.kie.api.definition.type.Label("GestionDTO")
public class GestionDTO implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label(value = "apruebaDemanda")
	private java.lang.String apruebaDemanda;
	@org.kie.api.definition.type.Label(value = "observaciones")
	private java.lang.String observaciones;
	@org.kie.api.definition.type.Label(value = "apruebaAccion")
	private java.lang.String apruebaAccion;
	@org.kie.api.definition.type.Label(value = "documento")
	private java.lang.String documento;
	@org.kie.api.definition.type.Label(value = "observacionSupervicion")
	private java.lang.String observacionSupervicion;
	@org.kie.api.definition.type.Label(value = "abogado")
	private com.sdis.poc.modelo.dto.ParticipanteDTO abogado;
	@org.kie.api.definition.type.Label(value = "supervisor")
	private com.sdis.poc.modelo.dto.ParticipanteDTO supervisor;

	public GestionDTO() {
	}

	public java.lang.String getApruebaDemanda() {
		return this.apruebaDemanda;
	}

	public void setApruebaDemanda(java.lang.String apruebaDemanda) {
		this.apruebaDemanda = apruebaDemanda;
	}

	public java.lang.String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(java.lang.String observaciones) {
		this.observaciones = observaciones;
	}

	public java.lang.String getApruebaAccion() {
		return this.apruebaAccion;
	}

	public void setApruebaAccion(java.lang.String apruebaAccion) {
		this.apruebaAccion = apruebaAccion;
	}

	public java.lang.String getDocumento() {
		return this.documento;
	}

	public void setDocumento(java.lang.String documento) {
		this.documento = documento;
	}

	public java.lang.String getObservacionSupervicion() {
		return this.observacionSupervicion;
	}

	public void setObservacionSupervicion(
			java.lang.String observacionSupervicion) {
		this.observacionSupervicion = observacionSupervicion;
	}

	public com.sdis.poc.modelo.dto.ParticipanteDTO getAbogado() {
		return this.abogado;
	}

	public void setAbogado(com.sdis.poc.modelo.dto.ParticipanteDTO abogado) {
		this.abogado = abogado;
	}

	public com.sdis.poc.modelo.dto.ParticipanteDTO getSupervisor() {
		return this.supervisor;
	}

	public void setSupervisor(com.sdis.poc.modelo.dto.ParticipanteDTO supervisor) {
		this.supervisor = supervisor;
	}

	public GestionDTO(java.lang.String apruebaDemanda,
			java.lang.String observaciones, java.lang.String apruebaAccion,
			java.lang.String documento,
			java.lang.String observacionSupervicion,
			com.sdis.poc.modelo.dto.ParticipanteDTO abogado,
			com.sdis.poc.modelo.dto.ParticipanteDTO supervisor) {
		this.apruebaDemanda = apruebaDemanda;
		this.observaciones = observaciones;
		this.apruebaAccion = apruebaAccion;
		this.documento = documento;
		this.observacionSupervicion = observacionSupervicion;
		this.abogado = abogado;
		this.supervisor = supervisor;
	}

}