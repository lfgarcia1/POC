package com.sdis.poc.modelo.negocio;

/**
 * This class was automatically generated by the data modeler tool.
 */

@org.kie.api.definition.type.Label("DatosContactoBO")
public class DatosContactoBO implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label("email")
	private java.lang.String email;
	@org.kie.api.definition.type.Label("direccion")
	private java.lang.String direccion;
	@org.kie.api.definition.type.Label("telefono")
	private java.lang.String telefono;

	public DatosContactoBO() {
	}

	public java.lang.String getEmail() {
		return this.email;
	}

	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	public java.lang.String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(java.lang.String direccion) {
		this.direccion = direccion;
	}

	public java.lang.String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(java.lang.String telefono) {
		this.telefono = telefono;
	}

	public DatosContactoBO(java.lang.String email, java.lang.String direccion,
			java.lang.String telefono) {
		this.email = email;
		this.direccion = direccion;
		this.telefono = telefono;
	}

}